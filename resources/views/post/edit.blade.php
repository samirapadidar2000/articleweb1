 <h1 class="display-3">Editing Stock</h1>
 <form method="post" action="{{ route('posts.update', $post->id) }}">
     @method('PATCH')
     @csrf
     <input type="text" name="post_title" value="{{ $post->title }}" /><br><br>
     <textarea name="post_body">{{ $post->body }}</textarea><br><br>
     <button type="submit">Update</button>
 </form>
