@extends('layouts.app')
@section('content')
<a href="{{ route('posts.create')}}">Add post</a>
<h4>Manage Posts</h4>
<table border="1">
    <thead>
        <th width="80px">Id</th>
        <th>Title</th>
        <th width="150px">READ</th>
        <th>DELETE</th>
        <th>EDIT</th>
    </thead>
    <tbody>
        @foreach($posts as $post)
        <tr>
            <td>{{ $post->id }}</td>
            <td>{{ $post->title }}</td>
            <td>
                <a href="{{ route('posts.show', $post->id) }}">READ</a>
            </td>
            <td>
                <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </td>
            <td><a href="{{ route('posts.edit',$post->id)}}">Edit</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
