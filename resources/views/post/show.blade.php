@extends('layouts.app')
@section('content')
<h3>MyWebtuts.com</h3>
<hr />
<br/>
<h3>{{ $post->title }}</h3>
<p>
    {{ $post->body }}
</p>
<hr />
<h4>Display Comments</h4>
<hr />
@include('post.commentsDisplay', ['comments' => $post->comments, 'post_id' => $post->id])
<h4>Add comment</h4>
<form method="post" action="{{ route('comments.store') }}">
    @csrf
    <textarea class="form-control" name="body"></textarea><br><br>
    <input type="hidden" name="post_id" value="{{ $post->id }}" />
    <input type="submit" value="Add Comment" /><br><br>
</form>
@endsection
