@foreach($comments as $comment)
    <div @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <strong>{{ $comment->user->name }}</strong>
        <p>{{ $comment->body }}</p>
        <form method="post" action="{{ route('comments.store') }}">
            @csrf
            <input type="text" name="body"/>
            <input type="hidden" name="post_id" value="{{ $post_id }}" />
            <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
            <input type="submit" value="Reply" />
            <hr />
        </form>
        @include('post.commentsDisplay', ['comments' => $comment->replies])
    </div>
@endforeach
