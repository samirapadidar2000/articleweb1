<?php

namespace App\Repository;

interface IPostRepository
{
    public function getAllPosts();
    public function getSinglePost($id);
    public function createPost(array $data);
    public function editPost($id);
    public function updatePost($id, array $data);
    public function deletePost($id);
}
