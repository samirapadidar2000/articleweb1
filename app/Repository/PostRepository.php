<?php

namespace App\Repository;

use App\Models\Post;

class PostRepository implements IPostRepository
{
    public function getAllPosts(){
        return Post::all();
    }

    public function getSinglePost($id)
    {
        return  Post::find($id);

    }

    public function createPost(array $data)
    {
        $post = new Post();
        $post->title = $data['title'];
        $post->body = $data['body'];
        $post->save();
    }

    public function editPost($id)
    {
        return Post::find($id);
    }

    public function updatePost($id, array $data)
    {
        Post::find($id)->update([
            'title' => $data['post_title'],
            'body' => $data['post_body']
        ]);
    }

    public function deletePost($id){
        $post = Post::find($id);
        $post->delete();
    }
}
