<?php
namespace App\Http\Controllers;
use App\Repository\IPostRepository;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
class PostController extends Controller
{
    /**
     * Write Your Code..
     *
     * @return string
     */
    public function __construct(IPostRepository $post){
        $this->post = $post;
    }

    public function index(){
        $posts = $this->post->getAllPosts();
        return view('post.index')->with('posts', $posts);
    }

    /**
     * Write Your Code..
     *
     * @return string
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Write Your Code..
     *
     * @return string
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $data = $request->all();
        $this->post->createPost($data);
        return redirect('/posts');
    }

    /**
     * Write Your Code..
     *
     * @return string
     */
    public function show($id)
    {
        $post = $this->post->getSinglePost($id);
        return view('post.show')->with('post', $post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post->deletePost($id);
        return redirect('/posts')->with('success', 'post removed.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->editPost($id);
        return view('post.edit')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'post_title' => 'required',
            'post_body' => 'required',
        ]);
        $data = $request->all();
        $this->post->updatePost($id, $data);
        return redirect('/posts');
    }
}
